
# How to start new project

1. create an new repo on gitlab <br />
1. clone the repo template-gulp (https://gitlab.com/phamthaivn/template-gulp.git)<br />
<code>git clone https://gitlab.com/phamthaivn/template-gulp.git \<name of new app> </code>
1. change remote URL of repo template-gulp to URL new repo <br />
<code>git remote set-url origin (link to new repo)</code>
1. go to (https://zeit.co/) create new app and link to the repo on git

# Require
1. An account (https://zeit.co)
